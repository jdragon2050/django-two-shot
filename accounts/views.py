from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import AccountForm
from django.contrib.auth.models import User
from django import forms
from .forms import SignUpForm

# Create your views here.
def login_view(request):
  if request.method == 'GET':
    form = AccountForm()
    return render(request, 'accounts/login.html', {'form': form})
  elif request.method == 'POST':
    form = AccountForm(request.POST)
    if form.is_valid():
      username = form.cleaned_data['username']
      password = form.cleaned_data['password']
      user = authenticate(username=username, password=password)
      if user is not None:
        login(request, user)
        return redirect('home')
      else:
        return render(request, 'accounts/login.html', {'form': form})
    else:
      return render(request, 'accounts/login.html', {'form': form})

def logout_view(request):
    logout(request)
    return redirect('login') 

def sign_up_view(request):
  if request.method == 'GET':
    form = SignUpForm()
    return render(request, 'accounts/signup.html', {'form': form})
  elif request.method == 'POST':
    form = SignUpForm(request.POST)
    if form.is_valid():
      username = form.cleaned_data['username']
      password = form.cleaned_data['password']
      password_confirmation = form.cleaned_data['password_confirmation']
      if password != password_confirmation:
        raise forms.ValidationError('the passwords do not match')
      user = User.objects.create_user(username=username, password=password)
      login(request, user)
      return redirect('home')
    else:
      return render(request, 'accounts/signup.html', {'form': form})
    
