from django.urls import path
from .views import show_receipts, create_receipt, show_categories, show_accounts, create_category, create_account

urlpatterns = [
    path('', show_receipts, name='show_receipts'),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', show_categories, name='category_list'),
    path('accounts/', show_accounts, name='account_list'),
    path('categories/create/', create_category, name='create_category'),
    path('accounts/create/', create_account, name='create_account'),
]
