from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth.decorators import login_required
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, CategoryForm, AccountForm
from django.http import HttpResponseRedirect
from django.urls import reverse

@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipts.html', {'receipts': receipts})

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False) 
            receipt.purchaser = request.user  
            receipt.save()  
            return redirect('home')
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})
@login_required
def show_categories(request):
    categories = ExpenseCategory.objects.all()
    for category in categories:
        category.total_receipts = Receipt.objects.filter(category=category).count()
    return render(request, 'receipts/categories.html', {'categories': categories})

@login_required
def show_accounts(request):
    accounts = Account.objects.all()
    for account in accounts:
        account.total_receipts = Receipt.objects.filter(account=account).count()
    return render(request, 'receipts/accounts.html', {'accounts': accounts})  

@login_required
def create_category(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        form = CategoryForm(request.POST)
        if form.is_valid():
          category = ExpenseCategory(name=name, owner=request.user)
          category.save()
          return redirect('category_list')
    else:
        form = CategoryForm()
    return render(request, 'receipts/create_category.html', {'form': form})

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    return render(request, 'receipts/create_account.html', {'form': form})
#